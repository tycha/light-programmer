use std::rc::Rc;

use criterion::{criterion_group, criterion_main, Criterion};
use light_programmer::{
    compiler::compile,
    parser::parse_script,
    utils::{SafeFloat, SpanExt},
};
use pest::Span;

fn benchmark(c: &mut Criterion) {
    c.bench_function("print", |b| b.iter(run));
}

fn run() {
    let script = include_str!("../script.ls");

    let parsed = parse_script(script).unwrap();

    let program = compile::<100>(parsed).unwrap();

    let beat_time_map = program.beat_time_map(181.).unwrap();

    for i in 0..30 * 60 * 3 {
        let frame = program
            .execute(
                &beat_time_map,
                Span::new(script, 0, script.len())
                    .unwrap()
                    .with(SafeFloat::try_new_no_span(i as f64 / 30.).unwrap()),
            )
            .unwrap();
    }
}

criterion_group!(benches, benchmark);
criterion_main!(benches);
