def bpm = 100

def r = [1, 0, 0]
def g = [0, 1, 0]
def b = [0, 0, 1]

create_array(100, fn() <r g b > .5 beats; switch)
