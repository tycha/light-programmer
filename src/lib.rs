#![feature(maybe_uninit_array_assume_init)]
#![feature(maybe_uninit_uninit_array)]
#![feature(array_try_map)]
#![feature(array_methods)]

use compiler::compile;
use parser::parse_script;
use pest::{error::Error, Span};

use crate::utils::{SafeFloat, SpanExt};

pub mod compiler;
pub mod interpreting;
pub mod parser;
mod stdlib;
pub mod utils;

pub const LIGHTS_AMT_VAR: &str = "lights_amt";
pub const GLOBAL_TIME_VAR: &str = "global_time";
pub const GLOBAL_BEAT_VAR: &str = "global_beat";
pub const LOCAL_TIME_VAR: &str = "local_time";
pub const LOCAL_BEAT_VAR: &str = "local_beat";
pub const BUILTINS: &[&str] = &[
    LIGHTS_AMT_VAR,
    GLOBAL_TIME_VAR,
    GLOBAL_BEAT_VAR,
    LOCAL_TIME_VAR,
    LOCAL_BEAT_VAR,
];

#[cfg(test)]
mod tests {
    use crate::compiler::compile;
    use crate::parser::parse_script;
    use pest::{error::Error, Span};

    use crate::utils::{SafeFloat, SpanExt};

    #[test]
    fn main() {
        // panic!("{:?}", run());
    }

    fn run() -> Result<(), Box<Error<crate::parser::Rule>>> {
        let script = include_str!("../script.ls");

        let parsed = parse_script(script)?;

        let program = compile::<100>(parsed)?;

        let beat_time_map = program.beat_time_map(6.)?;

        println!(
            "{:?}",
            program.execute(
                &beat_time_map,
                Span::new(script, 0, script.len())
                    .unwrap()
                    .with(SafeFloat::from_i32(5))
            )?
        );

        Ok(())
    }
}

fn run() -> Result<(), Box<Error<crate::parser::Rule>>> {
    let script = include_str!("../script.ls");

    let parsed = parse_script(script)?;

    let program = compile::<100>(parsed)?;

    let beat_time_map = program.beat_time_map(1.)?;

    println!(
        "{:?}",
        program.execute(
            &beat_time_map,
            Span::new(script, 0, script.len())
                .unwrap()
                .with(SafeFloat::from_i32(0))
        )?
    );

    Ok(())
}

#[cfg(test)]
pub mod test_utils {
    use std::rc::Rc;

    use pest::Span;

    use crate::{
        compiler::NativeFn,
        interpreting::{Closure, Value},
        utils::SafeFloat,
    };

    pub fn span(str: &str) -> Span<'_> {
        Span::new(str, 0, str.len()).unwrap()
    }

    pub fn expect_num(value: &Value<'_, '_>) -> f64 {
        match value {
            Value::Number(n) => n.inner(),
            _ => panic!("Expected a number"),
        }
    }

    pub fn expect_list<'a, 'b>(value: Value<'a, 'b>) -> Rc<[Value<'a, 'b>]> {
        match value {
            Value::List(n) => n,
            _ => todo!(),
        }
    }

    pub fn num(n: f64) -> Value<'static, 'static> {
        Value::Number(SafeFloat::try_new_no_span(n).unwrap())
    }

    pub fn list<'a, 'b, const AMT: usize>(l: [Value<'a, 'b>; AMT]) -> Value<'a, 'b> {
        Value::List(l.into())
    }

    pub fn closure(c: NativeFn) -> Value<'static, 'static> {
        Value::Closure(Closure::Native(c))
    }
}
