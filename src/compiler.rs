use std::{
    collections::BTreeMap,
    fmt::Debug,
    rc::{Rc, Weak},
    sync::atomic::{AtomicUsize, Ordering},
};

use pest::{error::Error, Span};

use crate::{
    interpreting::{BeatTimeMap, Closure, DefClosure, MaybeOwned, Query, Value, VarStack},
    parser::{Def, Expr, SequenceBlendType, Statements},
    stdlib::add_natives,
    utils::{error, RuntimeError, SafeFloat, SpanExt, WithSpan},
    BUILTINS, GLOBAL_BEAT_VAR, GLOBAL_TIME_VAR, LIGHTS_AMT_VAR, LOCAL_BEAT_VAR, LOCAL_TIME_VAR,
};

pub struct Program<'a> {
    program: WithSpan<'a, ClosureOrValue<'a>>,
    lights_amt: usize,
    bpm: WithSpan<'a, ClosureOrValue<'a>>,
}

impl<'a> Program<'a> {
    pub fn new(
        program: WithSpan<'a, ClosureOrValue<'a>>,
        bpm: WithSpan<'a, ClosureOrValue<'a>>,
        lights_amt: usize,
    ) -> Program<'a> {
        Program {
            program,
            lights_amt,
            bpm,
        }
    }

    pub fn beat_time_map(&self, duration: f64) -> Result<BeatTimeMap, RuntimeError<'a>> {
        BeatTimeMap::new(self.bpm.as_ref(), self.lights_amt, duration)
    }

    pub fn execute<'b>(
        &self,
        beat_time_map: &'b BeatTimeMap,
        time: WithSpan<'a, SafeFloat>,
    ) -> Result<Value<'a, 'b>, RuntimeError<'a>> {
        let beat = beat_time_map.query(time.map(Query::Time))?.beat();

        let stack = VarStack::new_base(beat, *time, SafeFloat::from_usize(self.lights_amt));

        self.program.execute(&stack, &beat_time_map)
    }
}

pub type NativeFn = for<'a, 'b> fn(
    args: &[Value<'a, 'b>],
    spans: &[Span<'a>],
    call_site: Span<'a>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>>;

#[derive(Clone, Debug)]
pub enum FnSourceType<'a> {
    Def(Rc<Box<dyn Instruction<'a> + 'a>>),
    Recursive(Weak<Box<dyn Instruction<'a> + 'a>>),
}

#[derive(Clone, Debug)]
pub struct FnSource<'a> {
    pub expected_params: Rc<[WithSpan<'a, &'a str>]>,
    pub fn_type: FnSourceType<'a>,
    pub id: usize,
}

#[derive(Clone, Copy, Debug)]
pub enum VarAccess {
    Propogating(usize),
    Scoped { source_id: usize, idx: usize },
}

pub fn compile<'a, const LIGHTS_AMT: usize>(
    statements: WithSpan<'a, Statements<'a>>,
) -> Result<Program<'a>, Box<Error<crate::parser::Rule>>> {
    let span = statements.span();

    let mut scope = Scope::new();
    add_natives(&mut scope);

    let id_counter = IdCounter::new();

    // It's impossible for a builtin add_var to return Err
    scope
        .add_var::<true>(
            span.with(LIGHTS_AMT_VAR),
            VarAccess::Scoped {
                source_id: 0,
                idx: 0,
            },
        )
        .unwrap();
    scope
        .add_var::<true>(
            span.with(GLOBAL_TIME_VAR),
            VarAccess::Scoped {
                source_id: 0,
                idx: 1,
            },
        )
        .unwrap();
    scope
        .add_var::<true>(
            span.with(GLOBAL_BEAT_VAR),
            VarAccess::Scoped {
                source_id: 0,
                idx: 2,
            },
        )
        .unwrap();
    scope
        .add_var::<true>(span.with(LOCAL_TIME_VAR), VarAccess::Propogating(0))
        .unwrap();
    scope
        .add_var::<true>(span.with(LOCAL_BEAT_VAR), VarAccess::Propogating(1))
        .unwrap();

    let compiled = span.with(compile_statements(
        statements.into_val(),
        &mut scope,
        &id_counter,
    )?);

    let bpm = match scope.resolve("bpm") {
        None => return Err(error("Expected a bpm def to exist", span)),
        Some(ScopeItem::Fn(bpm)) => ClosureOrValue::Closure(call_def_instruction(
            ClosuresOrValues::Values(Box::from([])),
            bpm,
        )),
        Some(ScopeItem::Native(bpm)) => ClosureOrValue::Closure(Box::from(Native {
            args: Box::new([]),
            spans: Box::new([]),
            func: bpm,
            call_site: span,
        })),
        Some(ScopeItem::OptimizeAs(val)) => ClosureOrValue::Value(val),
        Some(ScopeItem::Var(_)) => unreachable!(),
    };

    Ok(Program::new(compiled, span.with(bpm), LIGHTS_AMT))
}

#[derive(Clone)]
enum ScopeItem<'a> {
    Fn(FnSource<'a>),
    Native(NativeFn),
    Var(VarAccess),
    OptimizeAs(Value<'a, 'a>),
}

pub struct Scope<'this, 'a: 'this> {
    fns: BTreeMap<&'a str, ScopeItem<'a>>,
    prev: Option<&'this Scope<'this, 'a>>,
}

// FnOnce won't work because I can't express that 'a: 'b if 'b is higher ranked >:-(
trait AddRecursiveCallback<'a> {
    fn callback<'scope>(
        self,
        scope: &mut Scope<'scope, 'a>,
    ) -> Result<ClosureOrValue<'a>, Box<Error<crate::parser::Rule>>>
    where
        'a: 'scope;
}

impl<'this, 'a: 'this> Scope<'this, 'a> {
    fn new() -> Scope<'this, 'a> {
        Scope {
            fns: BTreeMap::new(),
            prev: None,
        }
    }

    fn push(&'this self) -> Scope<'this, 'a> {
        Scope {
            fns: BTreeMap::new(),
            prev: Some(self),
        }
    }

    fn add<const BUILTIN: bool>(
        &mut self,
        name: WithSpan<'a, &'a str>,
        value: ScopeItem<'a>,
    ) -> Result<(), Box<Error<crate::parser::Rule>>> {
        if !BUILTIN && BUILTINS.contains(&name) {
            return Err(error("Can't redefine a builtin variable", name.span()));
        }

        self.fns.insert(*name, value);

        Ok(())
    }

    fn add_var<const BUILTIN: bool>(
        &mut self,
        name: WithSpan<'a, &'a str>,
        access: VarAccess,
    ) -> Result<(), Box<Error<crate::parser::Rule>>> {
        self.add::<BUILTIN>(name, ScopeItem::Var(access))
    }

    pub fn add_native(&mut self, name: &'static str, native: NativeFn) {
        self.fns.insert(name, ScopeItem::Native(native));
    }

    fn add_recursive<F>(
        &mut self,
        name: WithSpan<'a, &'a str>,
        expected_params: Rc<[WithSpan<'a, &'a str>]>,
        id: usize,
        f: F,
    ) -> Result<(), Box<Error<crate::parser::Rule>>>
    where
        F: AddRecursiveCallback<'a>,
    {
        let mut maybe_err = None;
        let mut maybe_optimize_as = None;

        let params_cloned = expected_params.to_owned();

        let func = {
            let mut new_scope = self.push();

            Rc::new_cyclic(|weak| {
                if let Err(e) = new_scope.add::<false>(
                    name,
                    ScopeItem::Fn(FnSource {
                        id,
                        expected_params,
                        fn_type: FnSourceType::Recursive(weak.to_owned()),
                    }),
                ) {
                    maybe_err = Some(e);

                    // Return dummy value
                    return Box::new(ValueInstruction(Value::Number(SafeFloat::from_i32(0))));
                }

                match f.callback(&mut new_scope) {
                    Ok(ClosureOrValue::Value(v)) => {
                        maybe_optimize_as = Some(v);

                        // Return dummy value
                        Box::new(ValueInstruction(Value::Number(SafeFloat::from_i32(0))))
                    }
                    Ok(ClosureOrValue::Closure(v)) => v,
                    Err(e) => {
                        maybe_err = Some(e);

                        // Return dummy value
                        Box::new(ValueInstruction(Value::Number(SafeFloat::from_i32(0))))
                    }
                }
            })
        };

        if let Some(err) = maybe_err {
            return Err(err);
        }

        if let Some(optimized) = maybe_optimize_as {
            self.add::<false>(name, ScopeItem::OptimizeAs(optimized))
        } else {
            self.add::<false>(
                name,
                ScopeItem::Fn(FnSource {
                    expected_params: params_cloned,
                    fn_type: FnSourceType::Def(func),
                    id,
                }),
            )
        }
    }

    fn resolve(&self, name: &str) -> Option<ScopeItem<'a>> {
        self.fns
            .get(name)
            .map(Clone::clone)
            .or_else(|| self.prev.and_then(|prev| prev.resolve(name)))
    }

    fn resolve_err(
        &self,
        name: WithSpan<'a, &str>,
    ) -> Result<ScopeItem<'a>, Box<Error<crate::parser::Rule>>> {
        self.resolve(&name)
            .ok_or_else(|| error("This identifier wasn't found in this scope", name.span()))
    }
}

struct IdCounter(AtomicUsize);

impl IdCounter {
    fn new() -> IdCounter {
        IdCounter(AtomicUsize::new(1))
    }

    fn new_id(&self) -> usize {
        self.0.fetch_add(1, Ordering::Relaxed)
    }
}

// Fn won't work because I can't express that 'a: 'b if 'b is higher ranked >:-(
pub trait Instruction<'a>: Debug {
    fn execute<'s, 'b>(
        &self,
        stack: &'s VarStack<'a, 's, 'b>,
        beat_time_map: &'b BeatTimeMap,
    ) -> Result<Value<'a, 'b>, RuntimeError<'a>>;
}

macro_rules! instruction {
    (< $a: lifetime, $s: lifetime, $b: lifetime > $name: ty | $self: ident, $stack: pat_param, $beat_time_map: pat_param | $expr: expr) => {
        impl<$a> Instruction<$a> for $name {
            fn execute<$s, $b>(
                & $self,
                $stack: & $s VarStack<$a, $s, $b>,
                $beat_time_map: & $b BeatTimeMap
            ) -> Result<Value<$a, $b>, RuntimeError<$a>> {
                $expr
            }
        }
    };
}

#[derive(Debug)]
pub enum ClosureOrValue<'a> {
    Value(Value<'a, 'a>),
    Closure(Box<dyn Instruction<'a> + 'a>),
}

impl<'a> ClosureOrValue<'a> {
    fn to_instruction(self) -> Box<dyn Instruction<'a> + 'a> {
        match self {
            ClosureOrValue::Value(v) => Box::from(ValueInstruction(v)),
            ClosureOrValue::Closure(c) => c,
        }
    }

    pub fn execute<'b>(
        &self,
        stack: &VarStack<'a, '_, 'b>,
        beat_time_map: &'b BeatTimeMap,
    ) -> Result<Value<'a, 'b>, RuntimeError<'a>> {
        match self {
            ClosureOrValue::Value(v) => Ok(v.to_owned()),
            ClosureOrValue::Closure(c) => c.execute(stack, beat_time_map),
        }
    }
}

enum ClosuresOrValues<'a> {
    Values(Box<[Value<'a, 'a>]>),
    Closures(Box<[Box<dyn Instruction<'a> + 'a>]>),
}

impl<'a> ClosuresOrValues<'a> {
    fn from_closure_or_value_list(list: Vec<ClosureOrValue<'a>>) -> ClosuresOrValues<'a> {
        if list.iter().all(|v| matches!(v, ClosureOrValue::Value(_))) {
            ClosuresOrValues::Values(
                list.into_iter()
                    .map(|v| match v {
                        ClosureOrValue::Value(v) => v,
                        ClosureOrValue::Closure(_) => unreachable!(),
                    })
                    .collect(),
            )
        } else {
            ClosuresOrValues::Closures(
                list.into_iter()
                    .map(ClosureOrValue::to_instruction)
                    .collect(),
            )
        }
    }
}

#[derive(Debug)]
struct ValueInstruction<'a>(Value<'a, 'a>);

instruction!(<'a, 's, 'b> ValueInstruction<'a> |self, _, _| Ok(self.0.to_owned()));

#[derive(Debug)]
struct ConstructList<'a>(Box<[Box<dyn Instruction<'a> + 'a>]>);

instruction!(<'a, 's, 'b> ConstructList<'a> |self, stack, beat_time_map| {
    Ok(Value::List(
        self.0
            .iter()
            .map(|value| value.execute(stack, beat_time_map))
            .collect::<Result<Rc<_>, _>>()?,
    ))
});

#[derive(Debug)]
struct Native<'a> {
    args: Box<[Box<dyn Instruction<'a> + 'a>]>,
    spans: Box<[Span<'a>]>,
    call_site: Span<'a>,
    func: NativeFn,
}

instruction!(<'a, 's, 'b> Native<'a> |self, stack, beat_time_map| {
    let args = self
        .args
        .iter()
        .map(|value| value.execute(stack, beat_time_map))
        .collect::<Result<Box<[_]>, _>>()?;

    (self.func)(&*args, &self.spans, self.call_site).map(|v| v.into_val())
});

#[derive(Debug)]
struct CloseFn<'a>(FnSource<'a>);

instruction!(<'a, 's, 'b> CloseFn<'a> |self, stack, beat_time_map| {
    Ok(Value::Closure(Closure::Def(Rc::new(DefClosure {
        stack: stack.close(),
        func: self.0.to_owned(),
        beat_time_map,
    }))))
});

#[derive(Debug)]
struct CloseVar(VarAccess);

instruction!(<'a, 's, 'b> CloseVar |self, stack, _| {
    Ok(Value::Closure(Closure::OptimizeAs(Rc::new(
        stack.resolve(self.0).to_owned(),
    ))))
});

#[derive(Debug)]
struct AccessVar(VarAccess);

instruction!(<'a, 's, 'b> AccessVar |self, stack, _| Ok(stack.resolve(self.0).to_owned()));

#[derive(Debug)]
struct CallDefValues<'a> {
    def: Rc<Box<dyn Instruction<'a> + 'a>>,
    id: usize,
    args: Box<[Value<'a, 'a>]>,
}

instruction!(<'a, 's, 'b> CallDefValues<'a> |self, stack, beat_time_map| {
    let new_stack = stack.push(self.id, MaybeOwned::Borrowed(&self.args), None);

    self.def.execute(&new_stack, beat_time_map)
});

#[derive(Debug)]
struct CallDefValuesRecursive<'a> {
    def: Weak<Box<dyn Instruction<'a> + 'a>>,
    id: usize,
    args: Box<[Value<'a, 'a>]>,
}

instruction!(<'a, 's, 'b> CallDefValuesRecursive<'a> |self, stack, beat_time_map| {
    let new_stack = stack.push(self.id, MaybeOwned::Borrowed(&self.args), None);

    self.def
        .upgrade()
        .expect("The weak reference to exist")
        .execute(&new_stack, beat_time_map)
});

#[derive(Debug)]
struct CallDefClosures<'a> {
    def: Rc<Box<dyn Instruction<'a> + 'a>>,
    id: usize,
    args: Box<[Box<dyn Instruction<'a> + 'a>]>,
}

instruction!(<'a, 's, 'b> CallDefClosures<'a> |self, stack, beat_time_map| {
    let new_stack = stack.push(
        self.id,
        MaybeOwned::Owned(
            self.args
                .iter()
                .map(|v| v.execute(stack, beat_time_map))
                .collect::<Result<_, _>>()?,
        ),
        None,
    );

    self.def.execute(&new_stack, beat_time_map)
});

#[derive(Debug)]
struct CallDefClosuresRecursive<'a> {
    def: Weak<Box<dyn Instruction<'a> + 'a>>,
    id: usize,
    args: Box<[Box<dyn Instruction<'a> + 'a>]>,
}

instruction!(<'a, 's, 'b> CallDefClosuresRecursive<'a> |self, stack, beat_time_map| {
    let new_stack = stack.push(
        self.id,
        MaybeOwned::Owned(
            self.args
                .iter()
                .map(|v| v.execute(stack, beat_time_map))
                .collect::<Result<_, _>>()?,
        ),
        None,
    );

    self.def
        .upgrade()
        .expect("The weak reference to exist")
        .execute(&new_stack, beat_time_map)
});

fn call_def_instruction<'a>(
    args: ClosuresOrValues<'a>,
    def: FnSource<'a>,
) -> Box<dyn Instruction<'a> + 'a> {
    let id = def.id;

    match (args, def.fn_type) {
        (ClosuresOrValues::Values(values), FnSourceType::Def(def)) => Box::from(CallDefValues {
            def,
            id,
            args: values,
        }),
        (ClosuresOrValues::Values(values), FnSourceType::Recursive(def)) => {
            Box::from(CallDefValuesRecursive {
                def,
                id,
                args: values,
            })
        }
        (ClosuresOrValues::Closures(closures), FnSourceType::Def(def)) => {
            Box::from(CallDefClosures {
                def,
                id,
                args: closures,
            })
        }
        (ClosuresOrValues::Closures(closures), FnSourceType::Recursive(def)) => {
            Box::from(CallDefClosuresRecursive {
                def,
                id,
                args: closures,
            })
        }
    }
}

#[derive(Debug)]
struct CompiledElement<'a> {
    expr: WithSpan<'a, ClosureOrValue<'a>>,
    time: WithSpan<'a, ClosureOrValue<'a>>,
}

fn resolve_sequence_vars<'a, 's, 'b>(
    stack: &VarStack<'a, 's, 'b>,
    local_beat_access: VarAccess,
    span: Span<'a>,
) -> (SafeFloat, SafeFloat, SafeFloat) {
    let local_beat = Value::expect_number(stack.resolve(local_beat_access), span)
        .expect("Local beat to be a number");

    // See interpreting::new_base
    let global_beat = Value::expect_number(
        stack.resolve(VarAccess::Scoped {
            source_id: 0,
            idx: 2,
        }),
        span,
    )
    .expect("Global beat to be a number");

    let global_time = Value::expect_number(
        stack.resolve(VarAccess::Scoped {
            source_id: 0,
            idx: 1,
        }),
        span,
    )
    .expect("Global time to be a number");

    (local_beat, global_beat, global_time)
}

#[derive(Debug)]
struct SwitchSequence<'a> {
    elements: Box<[CompiledElement<'a>]>,
    local_beat_access: VarAccess,
    call_site: Span<'a>,
}

instruction!(<'a, 's, 'b> SwitchSequence<'a> |self, stack, beat_time_map| {
    let (local_beat, global_beat, global_time) =
        resolve_sequence_vars(stack, self.local_beat_access, self.call_site);

    let mut seq_end_beat = self.call_site.with(SafeFloat::from_i32(0));

    for element in &*self.elements {
        let time = Value::expect_number(
            &element.time.execute(stack, beat_time_map)?,
            element.time.span(),
        )?;

        let seq_start_beat = seq_end_beat;

        seq_end_beat = seq_end_beat.map_float(|v| v + time.inner())?;

        if seq_start_beat.into_val() <= local_beat && local_beat < seq_end_beat.into_val() {
            let new_stack = stack.push_with_local_beat_and_time(
                local_beat,
                seq_start_beat,
                global_beat,
                global_time,
                beat_time_map,
            )?;

            return element.expr.execute(&new_stack, beat_time_map);
        }
    }

    unreachable!("There should be a final element that lasts forever");
});

#[derive(Debug)]
struct AddSequence<'a> {
    elements: Box<[CompiledElement<'a>]>,
    local_beat_access: VarAccess,
    call_site: Span<'a>,
}

instruction!(<'a, 's, 'b> AddSequence<'a> |self, stack, beat_time_map| {
    let (local_beat, global_beat, global_time) =
        resolve_sequence_vars(stack, self.local_beat_access, self.call_site);

    let mut seq_end_beat = self.call_site.with(SafeFloat::from_i32(0));

    let mut sum: Option<SafeFloat> = None;

    for element in &*self.elements {
        let time = Value::expect_number(
            &element.time.execute(stack, beat_time_map)?,
            element.time.span(),
        )?;

        let seq_start_beat = seq_end_beat;

        seq_end_beat = seq_end_beat.map_float(|v| v + time.inner())?;

        if seq_start_beat.into_val() <= local_beat {
            let new_stack = stack.push_with_local_beat_and_time(
                local_beat,
                seq_start_beat,
                global_beat,
                global_time,
                beat_time_map,
            )?;

            let val = element.expr.execute(&new_stack, beat_time_map)?;

            let val = Value::expect_number(&val, element.expr.span())?;

            match sum {
                Some(s) => {
                    sum = Some(
                        SafeFloat::try_new_no_span(s.inner() + val.inner())
                            .map_err(|e| element.expr.span().with(e))?,
                    )
                }
                None => sum = Some(val),
            }
        } else {
            return Ok(Value::Number(sum.expect(
                "the sum to have been assigned in a previous iteration of the loop",
            )));
        }
    }

    Ok(Value::Number(
        sum.expect("the sum to have been assigned in the loop"),
    ))
});

fn compile_closure<'a, 'scope>(
    expr: WithSpan<'a, Expr<'a>>,
    scope: &Scope<'scope, 'a>,
    id_counter: &IdCounter,
) -> Result<ClosureOrValue<'a>, Box<Error<crate::parser::Rule>>> {
    let (expr, span) = expr.into_inner();

    match expr {
        Expr::Sequence(seq) => {
            let elements = seq
                .elements
                .into_iter()
                .map(|v| {
                    // Don't create a new scope for local_time and local_beat because they're propogated
                    Ok(CompiledElement {
                        expr: v
                            .expr
                            .span()
                            .with(compile_closure(v.expr, &scope, id_counter)?),
                        time: v
                            .time
                            .span()
                            .with(compile_closure(v.time, &scope, id_counter)?),
                    })
                })
                .collect::<Result<Box<[_]>, Box<Error<_>>>>()?;

            let local_beat_access = match scope
                .resolve(LOCAL_BEAT_VAR)
                .expect("Local beat to be defined")
            {
                ScopeItem::Var(access) => access,
                _ => panic!("Local beat should be defined like a parameter"),
            };

            Ok(match seq.blend_type.into_val() {
                SequenceBlendType::Switch => ClosureOrValue::Closure(Box::from(SwitchSequence {
                    elements,
                    local_beat_access,
                    call_site: span,
                })),
                SequenceBlendType::Add => ClosureOrValue::Closure(Box::from(AddSequence {
                    elements,
                    local_beat_access,
                    call_site: span,
                })),
            })
        }
        Expr::FnCall(fn_call) => {
            let resolved = scope.resolve_err(fn_call.name)?;

            let spans = fn_call
                .args
                .iter()
                .map(|v| v.span())
                .collect::<Box<[Span<'a>]>>();

            let args = fn_call
                .args
                .into_iter()
                .map(|expr| compile_closure(expr, scope, id_counter))
                .collect::<Result<Vec<_>, _>>()?;

            let args = ClosuresOrValues::from_closure_or_value_list(args);

            use ClosureOrValue as R;
            use ClosuresOrValues as C;
            use ScopeItem as S;

            match (resolved, args) {
                (S::Fn(def), args) => Ok(R::Closure(call_def_instruction(args, def))),
                (S::Native(native), C::Values(values)) => {
                    Ok(R::Value(native(&values, &spans, span)?.into_val()))
                }
                (S::Native(native), C::Closures(args)) => Ok(R::Closure(Box::from(Native {
                    args,
                    spans,
                    func: native,
                    call_site: span,
                }))),
                (S::Var(var_access), _) => Ok(R::Closure(Box::from(AccessVar(var_access)))),
                (S::OptimizeAs(value), _) => Ok(R::Value(value)),
            }
        }
        Expr::Statements(statements) => {
            let mut new_scope = scope.push();

            compile_statements(statements, &mut new_scope, id_counter)
        }
        Expr::Number(num) => Ok(ClosureOrValue::Value(Value::Number(
            SafeFloat::try_new_no_span(num).map_err(|e| RuntimeError::from(span.with(e)))?,
        ))),
        Expr::List(list) => {
            let list = list
                .into_iter()
                .map(|expr| compile_closure(expr, scope, id_counter))
                .collect::<Result<Vec<_>, _>>()?;

            match ClosuresOrValues::from_closure_or_value_list(list) {
                ClosuresOrValues::Values(list) => {
                    Ok(ClosureOrValue::Value(Value::List(list.into())))
                }
                ClosuresOrValues::Closures(closures) => {
                    Ok(ClosureOrValue::Closure(Box::from(ConstructList(closures))))
                }
            }
        }
        Expr::UnaryExpr(expr) => {
            let native = expr.op.native();

            let expr_span = expr.expr.span();
            let compiled = compile_closure(expr.expr, scope, id_counter)?;

            match compiled {
                ClosureOrValue::Value(value) => match native(&[value], &[expr_span], span) {
                    Ok(v) => Ok(ClosureOrValue::Value(v.into_val())),
                    Err(e) => Err(e.into()),
                },
                ClosureOrValue::Closure(closure) => {
                    Ok(ClosureOrValue::Closure(Box::from(Native {
                        args: Box::from([closure]),
                        spans: Box::from([expr_span]),
                        call_site: span,
                        func: native,
                    })))
                }
            }
        }
        Expr::BinaryExpr(expr) => {
            let native = expr.op.native();

            let lhs_span = expr.lhs.span();
            let rhs_span = expr.rhs.span();

            let lhs = compile_closure(expr.lhs, scope, id_counter)?;
            let rhs = compile_closure(expr.rhs, scope, id_counter)?;

            use ClosureOrValue as C;

            match (lhs, rhs) {
                (C::Value(lhs), C::Value(rhs)) => {
                    match native(&[lhs, rhs], &[lhs_span, rhs_span], span) {
                        Ok(v) => Ok(ClosureOrValue::Value(v.into_val())),
                        Err(e) => Err(e.into()),
                    }
                }
                (lhs, rhs) => Ok(ClosureOrValue::Closure(Box::from(Native {
                    args: Box::from([lhs.to_instruction(), rhs.to_instruction()]),
                    spans: Box::from([lhs_span, rhs_span]),
                    call_site: span,
                    func: native,
                }))),
            }
        }
        Expr::Close(name) => match scope.resolve_err(name)? {
            ScopeItem::Fn(func) => Ok(ClosureOrValue::Closure(Box::new(CloseFn(func)))),
            ScopeItem::Native(native) => Ok(ClosureOrValue::Value(Value::Closure(
                crate::interpreting::Closure::Native(native),
            ))),
            ScopeItem::Var(var) => Ok(ClosureOrValue::Closure(Box::new(CloseVar(var)))),
            // Make sure the result is still a closure since that's what's expected
            ScopeItem::OptimizeAs(val) => Ok(ClosureOrValue::Value(Value::Closure(
                crate::interpreting::Closure::OptimizeAs(Rc::new(val)),
            ))),
        },
        Expr::Closure(closure) => {
            let mut new_scope = scope.push();

            let id = id_counter.new_id();

            for (idx, param) in closure.params.iter().enumerate() {
                new_scope.add_var::<false>(*param, VarAccess::Scoped { source_id: id, idx })?;
            }

            Ok(ClosureOrValue::Closure(Box::from(CloseFn(FnSource {
                expected_params: closure.params.into_val(),
                fn_type: FnSourceType::Def(Rc::new(
                    compile_closure(closure.expr, &new_scope, id_counter)?.to_instruction(),
                )),
                id,
            }))))
        }
    }
}

fn compile_statements<'a, 's>(
    statements: Statements<'a>,
    scope: &mut Scope<'s, 'a>,
    id_counter: &IdCounter,
) -> Result<ClosureOrValue<'a>, Box<Error<crate::parser::Rule>>> {
    for def_element in statements.defs {
        let def = def_element.into_val();

        let id = id_counter.new_id();

        let params_cloned = def.params.to_owned().into_val();

        struct Callback<'a, 'c>(Def<'a>, usize, &'c IdCounter);

        impl<'a, 'c> AddRecursiveCallback<'a> for Callback<'a, 'c> {
            fn callback<'b>(
                self,
                def_scope: &mut Scope<'b, 'a>,
            ) -> Result<ClosureOrValue<'a>, Box<pest::error::Error<crate::parser::Rule>>>
            where
                'a: 'b,
            {
                for (idx, param) in self.0.params.into_val().iter().enumerate() {
                    def_scope.add_var::<false>(
                        *param,
                        VarAccess::Scoped {
                            source_id: self.1,
                            idx,
                        },
                    )?;
                }

                compile_closure(self.0.expr, def_scope, self.2)
            }
        }

        scope.add_recursive(def.name, params_cloned, id, Callback(def, id, id_counter))?;
    }

    compile_closure(*statements.expr, &scope, id_counter)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::parse_script;
    use crate::test_utils::*;

    macro_rules! exec {
        (duration = $duration: expr; $($value: pat_param if time = $at: expr;)+ |> $script: expr) => {
            let parsed = parse_script($script).unwrap();

            let span = parsed.span();

            let program = compile::<100>(parsed).unwrap();

            let beat_time_map = program.beat_time_map($duration).unwrap();

            $(
                let v = program
                    .execute(&beat_time_map, SafeFloat::try_new(span.with($at)).unwrap());

                let $value = v
                    else { panic!("Failed to match pattern: {v:?}") };
            )+
        };
    }

    #[test]
    fn test_ops() {
        exec!(
            duration = 1.;
            Ok(value) if time = 0.; |>

            "
                def bpm = 100

                10 + -100
            "
        );

        assert_eq!(value, num(-90.));
    }

    #[test]
    fn test_def() {
        exec!(
            duration = 4.;
            Ok(a) if time = 0.;
            Ok(b) if time = 3.; |>

            "
                def bpm = 100

                def x(a, b) = 5 + a * b

                x(global_time, 3)
            "
        );

        assert_eq!(a, num(5.));
        assert_eq!(b, num(14.));
    }

    #[test]
    fn test_closures_and_sequences() {
        exec!(
            duration = 3.;
            Ok(a) if time = 0.1;
            Ok(b) if time = 1.1;
            Ok(c) if time = 2.1;
            |>

            "
                def bpm = 60

                def v(i) = 2 ^ i

                def k(c, i) = 2 * call(c, i)

                |
                    create_array(5, @v) : 1
                |
                    create_array(5, @pi) : 1
                |
                    create_array(5, fn(i) k(@v, i))
                | switch
            "
        );

        assert_eq!(a, list([num(1.), num(2.), num(4.), num(8.), num(16.)]));
        const PI: f64 = std::f64::consts::PI;
        assert_eq!(b, list([num(PI), num(PI), num(PI), num(PI), num(PI)]));
        assert_eq!(c, list([num(2.), num(4.), num(8.), num(16.), num(32.)]));
    }

    #[test]
    fn test_variable_bpm_and_add_sequence() {
        exec!(
            duration = 5.;
            Ok(a) if time = 0.;
            Ok(b) if time = 1.;
            Ok(c) if time = 2.;
            Ok(d) if time = 3.;
            |>

            "
                # This is super cursed, too bad
                def bpm = | 120 : 4 | 60 | add

                local_beat
            "
        );

        assert_eq!(a, num(0.));
        assert_eq!(b, num(2. + 2. * f64::EPSILON));
        assert_eq!(c, num(4. + 6. * f64::EPSILON));
        assert_eq!(d, num(7. - 0.05 + 38. * f64::EPSILON)); // -0.05 for the trapezoidal approximation
    }

    #[test]
    fn test_track() {
        exec!(
            duration = 5.;
            Ok(a) if time = 0.1;
            Ok(b) if time = 1.1;
            Ok(c) if time = 2.1;
            |>

            "
                def bpm = 60

                <1 2 3 > 0.5 beats; switch
            "
        );

        assert_eq!(a, num(1.));
        assert_eq!(b, num(2.));
        assert_eq!(c, num(3.));
    }

    #[test]
    fn test_local_beat_propagation() {
        exec!(
            duration = 5.;
            Ok(Value::Number(a1)) if time = 0.1;
            Ok(Value::Number(b1)) if time = 1.1;
            Ok(Value::Number(c1)) if time = 2.1;
            Ok(Value::Number(a2)) if time = 0.6;
            Ok(Value::Number(b2)) if time = 1.6;
            Ok(Value::Number(c2)) if time = 2.6;
            |>

            "
                def bpm = 60
            
                def f = e ^ local_beat

                <f f f > 0.5 beats; switch
            "
        );

        assert_eq!(a1.inner(), b1.inner() - f64::EPSILON);
        assert_eq!(b1.inner(), c1.inner() - f64::EPSILON);
        assert_eq!(a2.inner(), b2.inner() - 6. * f64::EPSILON);
        assert_eq!(b2.inner(), c2.inner() + 16. * f64::EPSILON);
    }
}
