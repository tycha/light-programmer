use std::{collections::HashSet, mem::MaybeUninit, rc::Rc};

use pest::Span;

use crate::{
    compiler::Scope,
    interpreting::Value,
    utils::{RuntimeError, RuntimeErrorType, SafeFloat, SpanExt, WithSpan},
};

pub fn add_natives(scope: &mut Scope<'_, '_>) {
    scope.add_native("pi", pi);
    scope.add_native("e", e);
    scope.add_native("call", call);
    scope.add_native("create_array", create_array);
    scope.add_native("sin", sin);
    scope.add_native("cos", cos);
    scope.add_native("ln", ln);
    scope.add_native("floor", floor);
    scope.add_native("ceil", ceil);
    scope.add_native("round", round);
    scope.add_native("sign", sign);
    scope.add_native("abs", abs);
    scope.add_native("log_base", log_base);
    scope.add_native("min", min);
    scope.add_native("max", max);
    scope.add_native("clamp", clamp);
    scope.add_native("lerp", lerp);
    scope.add_native("place_in", place_in);
    scope.add_native("hsl_to_rgb", hsl_to_rgb);
}

fn pi<'a, 'b>(
    _: &[Value<'a, 'b>],
    _: &[Span<'a>],
    call_site: Span<'a>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
    Ok(call_site.with(Value::Number(
        SafeFloat::try_new_no_span(std::f64::consts::PI).expect("pi to be a valid float"),
    )))
}

fn e<'a, 'b>(
    _: &[Value<'a, 'b>],
    _: &[Span<'a>],
    call_site: Span<'a>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
    Ok(call_site.with(Value::Number(
        SafeFloat::try_new_no_span(std::f64::consts::E).expect("e to be a valid float"),
    )))
}

fn create_array<'a, 'b>(
    args: &[Value<'a, 'b>],
    spans: &[Span<'a>],
    call_site: Span<'a>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
    let [maybe_len, maybe_callback] = extract_args(args, spans, call_site)?;

    let len = Value::expect_number(&maybe_len, maybe_len.span())?.inner() as usize;
    let callback = Value::expect_closure(maybe_callback.as_ref())?;

    let mut out = Vec::with_capacity(len);

    for i in 0..len {
        out.push(
            callback
                .execute(
                    &[Value::Number(SafeFloat::from_usize(i))],
                    &[maybe_len.span()],
                    call_site,
                )
                .map(WithSpan::into_val)?,
        );
    }

    Ok(call_site.with(Value::List(Rc::from(out))))
}

fn call<'a, 'b>(
    args: &[Value<'a, 'b>],
    spans: &[Span<'a>],
    call_site: Span<'a>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
    let (callback, args) = match args.split_first() {
        Some(v) => v,
        None => return Err(call_site.with(RuntimeErrorType::IncorrectArgs(0, 1)).into()),
    };

    let callback = match callback {
        Value::Closure(c) => c,
        v => {
            return Err(spans[0]
                .with(RuntimeErrorType::TypeError {
                    expected: "Closure".to_owned(),
                    got: v.ty().to_owned(),
                })
                .into())
        }
    };

    callback.execute(args, spans.split_at(1).1, call_site)
}

fn number_fn<'a, 'b, const AMT: usize>(
    func: impl Fn([f64; AMT]) -> f64,
) -> impl Fn([f64; AMT]) -> Result<Value<'a, 'b>, RuntimeErrorType> {
    move |v| SafeFloat::try_new_no_span(func(v)).map(|v| Value::Number(v))
}

macro_rules! number_op {
    ($name: ident: |$($arg: ident),*| $expr: expr) => {
        pub fn $name<'a, 'b>(
            args: &[Value<'a, 'b>],
            spans: &[Span<'a>],
            call_site: Span<'a>,
        ) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
            number_op_extract(args, spans, call_site, &number_fn(|[$($arg,)*]| $expr))
        }
    };
}

number_op!(neg: |v| -v);
number_op!(rem: |a, b| a.rem_euclid(b));
number_op!(add: |a, b| a + b);
number_op!(sub: |a, b| a - b);
number_op!(mul: |a, b| a * b);
number_op!(div: |a, b| a / b);
number_op!(exp: |a, b| a.powf(b));

number_op!(sin: |v| v.sin());
number_op!(cos: |v| v.cos());
number_op!(ln: |v| v.ln());
number_op!(ceil: |v| v.ceil());
number_op!(floor: |v| v.floor());
number_op!(round: |v| v.round());
number_op!(sign: |v| if v == 0. { 0. } else { v.signum() });
number_op!(abs: |v| v.abs());

number_op!(log_base: |val, base| val.log(base));
number_op!(min: |a, b| a.min(b));
number_op!(max: |a, b| a.max(b));

number_op!(clamp: |val, min, max| val.clamp(min, max));
number_op!(lerp: |val, min, max| min + val * (max - min));
number_op!(place_in: |val, min, max| (val - min) / (max - min));

fn extract_args<'a, 'v, 'b, const AMT: usize>(
    args: &[Value<'a, 'b>],
    spans: &[Span<'a>],
    call_site: Span<'a>,
) -> Result<[WithSpan<'a, Value<'a, 'b>>; AMT], RuntimeError<'a>> {
    if args.len() != AMT {
        return Err(RuntimeError::from(
            call_site.with(RuntimeErrorType::IncorrectArgs(AMT, args.len())),
        ));
    }

    if spans.len() != AMT {
        panic!("Expected the spans array to be the same size as the args");
    }

    let mut values = MaybeUninit::uninit_array();

    for (i, (arg, span)) in args.into_iter().zip(spans).enumerate() {
        values[i].write(span.with(arg.to_owned()));
    }

    // SAFETY: It was just initialized
    Ok(unsafe { MaybeUninit::array_assume_init(values) })
}

struct ArgsZipper<'a, 'v, 'b, const AMT: usize>(
    [WithSpan<'a, &'v Value<'a, 'b>>; AMT],
    usize,
    usize,
);

impl<'a, 'v, 'b, const AMT: usize> ArgsZipper<'a, 'v, 'b, AMT> {
    fn new(
        args: [WithSpan<'a, &'v Value<'a, 'b>>; AMT],
        call_site: Span<'a>,
    ) -> Result<Self, RuntimeError<'a>> {
        let lengths_set = args
            .iter()
            .filter_map(|v| match &**v {
                Value::Number(_) | Value::Closure(_) => None,
                Value::List(l) => Some(l.len()),
            })
            .collect::<HashSet<_>>();

        if lengths_set.len() > 1 {
            return Err(RuntimeError::from(
                call_site.with(RuntimeErrorType::ListsDifferentLengths(lengths_set)),
            ));
        }

        Ok(ArgsZipper(
            args,
            0,
            lengths_set.into_iter().next().unwrap_or(1), /*If there are no arrays, just return one value for the non-arrays*/
        ))
    }
}

impl<'a, 'v, 'b, const AMT: usize> Iterator for ArgsZipper<'a, 'v, 'b, AMT> {
    type Item = [WithSpan<'a, &'v Value<'a, 'b>>; AMT];

    fn next(&mut self) -> Option<Self::Item> {
        let i = self.1;

        if i == self.2 {
            // Iterator's over
            return None;
        }

        self.1 += 1;

        if self.0.len() != AMT {
            panic!("The length shouldn't change");
        }

        let mut out = MaybeUninit::uninit_array();

        for (k, arg) in self.0.into_iter().enumerate() {
            let (val, span) = arg.as_ref().into_inner();
            out[k] = MaybeUninit::new(span.with(match val {
                Value::List(l) => l.get(i).expect("the lengths should have been checked on initialization and the index shouldn't go over the checked length"),
                v => v,
            }));
        }

        // SAFETY: Every element was initialized in the previous loop because the length was checked
        Some(unsafe { MaybeUninit::array_assume_init(out) })
    }
}

fn number_op_extract<'a, 'b, const AMT: usize>(
    args: &[Value<'a, 'b>],
    spans: &[Span<'a>],
    call_site: Span<'a>,
    func: &impl Fn([f64; AMT]) -> Result<Value<'a, 'b>, RuntimeErrorType>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
    vectorize(
        extract_args(args, spans, call_site)?
            .each_ref()
            .map(|v| v.as_ref()),
        call_site,
        &|v| match func(v.try_map(|v| {
            match &*v {
                Value::Number(n) => Ok::<f64, RuntimeError<'a>>(n.inner()),
                _ => Err(v
                    .span()
                    .with(RuntimeErrorType::TypeError {
                        expected: "Number".to_owned(),
                        got: v.ty().to_owned(),
                    })
                    .into()),
            }
        })?) {
            Ok(v) => Ok(call_site.with(v)),
            Err(e) => Err(call_site.with(e).into()),
        },
    )
}

fn vectorize<'a, 'b, const AMT: usize>(
    args: [WithSpan<'a, &Value<'a, 'b>>; AMT],
    call_site: Span<'a>,
    func: &impl Fn(
        [WithSpan<'a, &Value<'a, 'b>>; AMT],
    ) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
    if args.iter().all(|v| matches!(**v, Value::Number(_))) {
        return func(args);
    }

    ArgsZipper::new(args, call_site)?
        .map(|v| vectorize(v, call_site, func).map(|v| v.into_val()))
        .collect::<Result<Rc<[_]>, _>>()
        .map(|v| call_site.with(Value::List(v)))
}

fn hsl_to_rgb<'a, 'b>(
    args: &[Value<'a, 'b>],
    spans: &[Span<'a>],
    call_site: Span<'a>,
) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
    fn hue_to_rgb(p: f64, q: f64, mut t: f64) -> f64 {
        if t < 0. {
            t += 1.
        }

        if t > 1. {
            t -= 1.
        }

        if t < 1. / 6. {
            return (q - p).mul_add(6. * t, p);
        }

        if t < 0.5 {
            return q;
        }

        if t < 2. / 3. {
            return (q - p).mul_add((2. / 3. - t) * 6., p);
        }

        p
    }

    if args.len() != 1 {
        return Err(call_site
            .with(RuntimeErrorType::IncorrectArgs(1, args.len()))
            .into());
    }

    let arg = args
        .into_iter()
        .next()
        .expect("The value was just checked")
        .to_owned();

    let span = spans
        .into_iter()
        .next()
        .expect("The spans and values to be the same length")
        .to_owned();

    let arg = match arg {
        Value::List(v) => v,
        Value::Number(_) => {
            return Err(call_site
                .with(RuntimeErrorType::TypeError {
                    expected: "List".to_owned(),
                    got: "Number".to_owned(),
                })
                .into())
        }
        Value::Closure(_) => {
            return Err(call_site
                .with(RuntimeErrorType::TypeError {
                    expected: "List".to_owned(),
                    got: "Closure".to_owned(),
                })
                .into())
        }
    };

    if arg.len() == 3 && arg.iter().all(|v| matches!(v, Value::Number(_))) {
        let args = arg
            .iter()
            .map(|v| match v {
                Value::Number(n) => *n,
                _ => unreachable!("Checked before"),
            })
            .collect::<Vec<_>>();

        let h = args[0];
        let s = args[1];
        let l = args[2];

        if s.inner() == 0. {
            return Ok(call_site.with(Value::List(Rc::from([
                Value::Number(l),
                Value::Number(l),
                Value::Number(l),
            ]))));
        }

        let h = h.inner();
        let s = s.inner();
        let l = l.inner();

        let q = if l < 0.5 {
            l * (1. + s)
        } else {
            l - l.mul_add(s, -s)
        };
        let p = l.mul_add(2., -q);

        return Ok(call_site.with(Value::List(Rc::from([
            Value::Number(
                SafeFloat::try_new(call_site.with(hue_to_rgb(p, q, h + 1. / 3.)))?.into_val(),
            ),
            Value::Number(SafeFloat::try_new(call_site.with(hue_to_rgb(p, q, h)))?.into_val()),
            Value::Number(
                SafeFloat::try_new(call_site.with(hue_to_rgb(p, q, h - 1. / 3.)))?.into_val(),
            ),
        ]))));
    }

    Ok(call_site.with(Value::List(
        arg.iter()
            .map(|v| hsl_to_rgb(&[v.to_owned()], &[span], call_site).map(|v| v.into_val()))
            .collect::<Result<Rc<[_]>, _>>()?,
    )))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utils::*;

    #[test]
    fn test_pi() {
        assert_eq!(
            expect_num(&pi(&[], &[], span("")).unwrap()),
            std::f64::consts::PI
        );
    }

    #[test]
    fn test_e() {
        assert_eq!(
            expect_num(&e(&[], &[], span("")).unwrap()),
            std::f64::consts::E
        );
    }

    #[test]
    fn test_call() {
        assert_eq!(
            expect_num(
                &call(
                    &[closure(mul), num(2.), num(9.)],
                    &[span("a"), span("b"), span("c")],
                    span("")
                )
                .unwrap()
            ),
            18.
        );
    }

    #[test]
    fn test_create_array() {
        assert_eq!(
            expect_list(
                create_array(
                    &[num(4.), closure(|v, _, s| { Ok(s.with(v[0].to_owned())) })],
                    &[span("a"), span("b")],
                    span("")
                )
                .unwrap()
                .into_val()
            )
            .iter()
            .map(|v| expect_num(v))
            .collect::<Vec<_>>(),
            &[0., 1., 2., 3.]
        );
    }

    #[test]
    fn test_neg() {
        assert_eq!(
            neg(&[num(4.)], &[span("")], span("")).unwrap().into_val(),
            num(-4.)
        );
    }

    #[test]
    fn test_add_and_vectorization() {
        assert_eq!(
            expect_num(&add(&[num(4.), num(5.)], &[span(""), span("")], span("")).unwrap()),
            9.
        );

        assert_eq!(
            add(
                &[
                    list([num(6.), num(4.), list([num(9.), num(1.)])]),
                    list([num(3.), num(-1.), list([num(-3.), num(4.)])])
                ],
                &[span(""), span("")],
                span("")
            )
            .unwrap()
            .into_val(),
            list([num(9.), num(3.), list([num(6.), num(5.)])])
        );

        assert_eq!(
            add(
                &[
                    list([num(6.), num(4.), list([num(9.), num(1.)])]),
                    list([num(3.), num(-1.), num(4.)])
                ],
                &[span(""), span("")],
                span("")
            )
            .unwrap()
            .into_val(),
            list([num(9.), num(3.), list([num(13.), num(5.)])])
        );

        assert!(add(
            &[
                list([num(6.), num(4.), list([num(9.), num(1.)])]),
                list([num(3.), num(-1.), list([num(-3.)])])
            ],
            &[span(""), span("")],
            span("")
        )
        .is_err());
    }

    #[test]
    fn test_rem() {
        assert_eq!(
            rem(&[num(5.), num(2.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(1.)
        );

        assert_eq!(
            rem(&[num(-4.), num(3.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(2.)
        );

        assert_eq!(
            rem(&[num(5.), num(-2.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(1.)
        );
    }

    #[test]
    fn test_sub() {
        assert_eq!(
            sub(&[num(3.), num(-5.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(8.)
        );
    }

    #[test]
    fn test_mul() {
        assert_eq!(
            mul(&[num(3.), num(-5.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(-15.)
        );
    }

    #[test]
    fn test_div() {
        assert_eq!(
            div(&[num(3.), num(-6.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(-0.5)
        );
    }

    #[test]
    fn test_exp() {
        assert_eq!(
            exp(&[num(3.), num(3.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(27.)
        );
    }

    #[test]
    fn test_log_base() {
        assert_eq!(
            log_base(&[num(16.), num(2.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(4.)
        );
    }

    #[test]
    fn test_min() {
        assert_eq!(
            min(&[num(16.), num(2.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(2.)
        );
    }

    #[test]
    fn test_max() {
        assert_eq!(
            max(&[num(16.), num(2.)], &[span("a"), span("b")], span(""))
                .unwrap()
                .into_val(),
            num(16.)
        );
    }

    #[test]
    fn test_clamp() {
        assert_eq!(
            clamp(
                &[num(16.), num(2.), num(18.)],
                &[span("a"), span("b"), span("c")],
                span("")
            )
            .unwrap()
            .into_val(),
            num(16.)
        );

        assert_eq!(
            clamp(
                &[num(16.), num(2.), num(14.)],
                &[span("a"), span("b"), span("c")],
                span("")
            )
            .unwrap()
            .into_val(),
            num(14.)
        );

        assert_eq!(
            clamp(
                &[num(-8.), num(2.), num(18.)],
                &[span("a"), span("b"), span("c")],
                span("")
            )
            .unwrap()
            .into_val(),
            num(2.)
        );
    }

    #[test]
    fn test_lerp() {
        assert_eq!(
            lerp(
                &[num(0.5), num(2.), num(18.)],
                &[span("a"), span("b"), span("c")],
                span("")
            )
            .unwrap()
            .into_val(),
            num(10.)
        );
    }

    #[test]
    fn test_place_in() {
        assert_eq!(
            place_in(
                &[num(10.), num(2.), num(18.)],
                &[span("a"), span("b"), span("c")],
                span("")
            )
            .unwrap()
            .into_val(),
            num(0.5)
        );
    }

    #[test]
    fn test_sin() {
        assert_eq!(
            sin(&[num(std::f64::consts::FRAC_PI_2)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(1.)
        );
    }

    #[test]
    fn test_cos() {
        assert_eq!(
            cos(&[num(std::f64::consts::PI)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(-1.)
        );
    }

    #[test]
    fn test_ln() {
        assert_eq!(
            ln(&[num(std::f64::consts::E)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(1.)
        );
    }

    #[test]
    fn test_ceil() {
        assert_eq!(
            ceil(&[num(1.5)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(2.)
        );
    }

    #[test]
    fn test_floor() {
        assert_eq!(
            floor(&[num(1.5)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(1.)
        );
    }

    #[test]
    fn test_round() {
        assert_eq!(
            round(&[num(1.5)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(2.)
        );
    }

    #[test]
    fn test_sign() {
        assert_eq!(
            sign(&[num(1.5)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(1.)
        );

        assert_eq!(
            sign(&[num(-2.5)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(-1.)
        );

        assert_eq!(
            sign(&[num(0.)], &[span("a")], span("")).unwrap().into_val(),
            num(0.)
        );
    }

    #[test]
    fn test_abs() {
        assert_eq!(
            abs(&[num(1.5)], &[span("a")], span("")).unwrap().into_val(),
            num(1.5)
        );

        assert_eq!(
            abs(&[num(-2.5)], &[span("a")], span(""))
                .unwrap()
                .into_val(),
            num(2.5)
        );
    }

    #[test]
    fn test_hsl_to_rgb() {
        assert_eq!(
            hsl_to_rgb(
                &[list([
                    list([num(0.75), num(1.), num(0.5)]),
                    list([num(0.5), num(0.), num(0.75)])
                ])],
                &[span("a")],
                span("")
            )
            .unwrap()
            .into_val(),
            list([
                list([num(0.5 - 2. * f64::EPSILON), num(0.), num(1.)]),
                list([num(0.75), num(0.75), num(0.75)])
            ])
        );
    }
}
