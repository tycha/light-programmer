use pest::Span;
use std::{fmt::Debug, rc::Rc};

use crate::{
    compiler::{ClosureOrValue, FnSource, NativeFn, VarAccess},
    utils::{RuntimeError, RuntimeErrorType, SafeFloat, SpanExt, WithSpan},
};

#[derive(Clone, PartialEq, Eq)]
pub enum Value<'a, 'b> {
    Number(SafeFloat),
    List(Rc<[Value<'a, 'b>]>),
    Closure(Closure<'a, 'b>),
}

impl<'a, 'b> Debug for Value<'a, 'b> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Number(n) => {
                f.write_str("Number(")?;
                n.fmt(f)?;
                f.write_str(")")
            }
            Value::List(l) => {
                f.write_str("List(")?;
                l.fmt(f)?;
                f.write_str(")")
            }
            Value::Closure(c) => {
                f.write_str("List(")?;
                c.fmt(f)?;
                f.write_str(")")
            }
        }
    }
}

impl<'a, 'b, T: Into<Value<'a, 'b>>> From<Vec<T>> for Value<'a, 'b> {
    fn from(value: Vec<T>) -> Self {
        Value::List(value.into_iter().map(|v| v.into()).collect())
    }
}

impl TryFrom<f64> for Value<'static, 'static> {
    type Error = RuntimeErrorType;

    fn try_from(value: f64) -> Result<Self, Self::Error> {
        Ok(Value::Number(SafeFloat::try_new_no_span(value)?))
    }
}

impl<'a, 'b> Value<'a, 'b> {
    pub fn ty(&self) -> &'static str {
        match self {
            Value::Number(_) => "Number",
            Value::List(_) => "List",
            Value::Closure(_) => "Closure",
        }
    }

    pub fn expect_number(
        this: &Value<'a, 'b>,
        span: Span<'a>,
    ) -> Result<SafeFloat, RuntimeError<'a>> {
        match this {
            Value::Number(n) => Ok(*n),
            v => Err(span
                .with(RuntimeErrorType::TypeError {
                    expected: "Number".to_owned(),
                    got: v.ty().to_owned(),
                })
                .into()),
        }
    }

    pub fn expect_closure<'v>(
        this: WithSpan<'a, &'v Value<'a, 'b>>,
    ) -> Result<WithSpan<'a, &'v Closure<'a, 'b>>, RuntimeError<'a>> {
        this.try_map(|v| match v {
            Value::Closure(n) => Ok(n),
            v => Err(this
                .span()
                .with(RuntimeErrorType::TypeError {
                    expected: "Number".to_owned(),
                    got: v.ty().to_owned(),
                })
                .into()),
        })
    }
}

pub struct DefClosure<'a, 'b> {
    pub stack: VarStack<'a, 'b, 'b>,
    pub func: FnSource<'a>,
    pub beat_time_map: &'b BeatTimeMap,
}

#[derive(Clone)]
pub enum Closure<'a, 'b> {
    Def(Rc<DefClosure<'a, 'b>>),
    Native(NativeFn),
    OptimizeAs(Rc<Value<'a, 'b>>),
}

impl<'a, 'b> PartialEq for Closure<'a, 'b> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            // Otherwise I would have to derive (PartialEq, Eq) for like the whole codebase just for tests. I don't think this is a ridiculous equality test anyways.
            (Closure::Def(d1), Closure::Def(d2)) => core::ptr::eq(&*d1, &*d2),
            (Closure::Native(n1), Closure::Native(n2)) => n1 == n2,
            (Closure::OptimizeAs(v1), Closure::OptimizeAs(v2)) => v1 == v2,
            _ => false,
        }
    }
}

impl<'a, 'b> Eq for Closure<'a, 'b> {}

impl<'a, 'b> Closure<'a, 'b> {
    pub fn execute(
        &self,
        args: &[Value<'a, 'b>],
        spans: &[Span<'a>],
        call_site: Span<'a>,
    ) -> Result<WithSpan<'a, Value<'a, 'b>>, RuntimeError<'a>> {
        match self {
            Closure::Def(def) => match &def.func.fn_type {
                crate::compiler::FnSourceType::Def(func) => {
                    let stack = def
                        .stack
                        .push(def.func.id, MaybeOwned::Borrowed(args), None);

                    func.execute(&stack, def.beat_time_map)
                        .map(|v| call_site.with(v))
                }
                crate::compiler::FnSourceType::Recursive(func) => {
                    let stack = def
                        .stack
                        .push(def.func.id, MaybeOwned::Borrowed(args), None);

                    func.upgrade()
                        .expect("The weak reference to exist")
                        .execute(&stack, def.beat_time_map)
                        .map(|v| call_site.with(v))
                }
            },
            Closure::Native(native) => native(args, &spans, call_site),
            Closure::OptimizeAs(val) => Ok(call_site.with((**val).to_owned())),
        }
    }
}

impl<'a, 'b> Debug for Closure<'a, 'b> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Closure::Def(def) => {
                f.write_str("Def(")?;
                def.func.fmt(f)?;
                f.write_str(")")
            }
            Closure::Native(_) => f.write_str("Native(*unknown*)"),
            Closure::OptimizeAs(v) => {
                f.write_str("OptimizeAs(")?;
                v.fmt(f)?;
                f.write_str(")")
            }
        }
    }
}

// [local_time, local_beat]
#[derive(Clone, Debug)]
pub struct PropogatingData<'a, 'b>([Value<'a, 'b>; 2]);

#[derive(Debug)]
pub enum MaybeOwned<'s, T: ?Sized> {
    Owned(Box<T>),
    Borrowed(&'s T),
}

#[derive(Debug)]
pub struct VarStack<'a, 's, 'b> {
    vars: MaybeOwned<'s, [Value<'a, 'b>]>,
    prev: Option<MaybeOwned<'s, VarStack<'a, 's, 'b>>>,
    propogating_data: Option<PropogatingData<'a, 'b>>,
    id: usize,
}

impl<'a, 's, 'b> VarStack<'a, 's, 'b> {
    pub fn new(
        id: usize,
        vars: MaybeOwned<'s, [Value<'a, 'b>]>,
        propogating_data: PropogatingData<'a, 'b>,
    ) -> VarStack<'a, 's, 'b> {
        VarStack {
            vars,
            prev: None,
            propogating_data: Some(propogating_data),
            id,
        }
    }

    pub fn push(
        &'s self,
        id: usize,
        vars: MaybeOwned<'s, [Value<'a, 'b>]>,
        propogating_data: Option<PropogatingData<'a, 'b>>,
    ) -> VarStack<'a, 's, 'b> {
        VarStack {
            vars,
            prev: Some(MaybeOwned::Borrowed(self)),
            propogating_data,
            id,
        }
    }

    pub fn resolve(&'s self, var_access: VarAccess) -> &'s Value<'a, 'b> {
        match var_access {
            VarAccess::Propogating(idx) => self.resolve_propogating(idx),
            VarAccess::Scoped { source_id, idx } => self.resolve_scoped(source_id, idx),
        }
    }

    fn resolve_propogating(&'s self, idx: usize) -> &'s Value<'a, 'b> {
        match &self.propogating_data {
            Some(v) => &v.0[idx],
            None => {
                let prev = self
                    .prev
                    .as_ref()
                    .expect("there to be propogating data somewhere");

                match prev {
                    MaybeOwned::Owned(v) => v,
                    MaybeOwned::Borrowed(v) => *v,
                }
                .resolve_propogating(idx)
            }
        }
    }

    fn resolve_scoped(&'s self, source_id: usize, idx: usize) -> &'s Value<'a, 'b> {
        if self.id == source_id {
            match &self.vars {
                MaybeOwned::Owned(v) => v,
                MaybeOwned::Borrowed(v) => *v,
            }
            .get(idx)
            .expect("invalid var accesses not to be compiled")
        } else {
            let prev = self
                .prev
                .as_ref()
                .expect("invalid var accesses not to be compiled");

            match prev {
                MaybeOwned::Owned(v) => v,
                MaybeOwned::Borrowed(v) => *v,
            }
            .resolve_scoped(source_id, idx)
        }
    }

    pub fn push_with_local_beat_and_time(
        &'s self,
        local_beat: SafeFloat,
        seq_start_beat: WithSpan<'a, SafeFloat>,
        global_beat: SafeFloat,
        global_time: SafeFloat,
        beat_time_map: &BeatTimeMap,
    ) -> Result<VarStack<'a, 's, 'b>, RuntimeError<'a>> {
        let local_to_elt_beat = seq_start_beat.map_float(|v| local_beat.inner() - v)?;

        let local_time = beat_time_map
            .query(
                local_to_elt_beat
                    .map_float(|v| global_beat.inner() - v)?
                    .map(Query::Beat),
            )?
            .time();

        Ok(self.push(
            usize::MAX,
            MaybeOwned::Borrowed(&[]),
            Some(PropogatingData([
                Value::Number(
                    SafeFloat::try_new_no_span(global_time.inner() - local_time.inner())
                        .map_err(|e| RuntimeError::from(seq_start_beat.span().with(e)))?,
                ),
                Value::Number(*local_to_elt_beat),
            ])),
        ))
    }

    pub fn new_base(
        global_beat: SafeFloat,
        global_time: SafeFloat,
        lights_amt: SafeFloat,
    ) -> VarStack<'a, 'a, 'a> {
        VarStack::new(
            0,
            MaybeOwned::Owned(Box::from([
                // See compiler::compile
                Value::Number(lights_amt),
                Value::Number(global_time),
                Value::Number(global_beat),
            ])),
            PropogatingData([Value::Number(global_time), Value::Number(global_beat)]),
        )
    }

    pub fn close(&self) -> VarStack<'a, 'b, 'b> {
        VarStack {
            vars: match &self.vars {
                MaybeOwned::Owned(v) => MaybeOwned::Owned(v.to_owned()),
                MaybeOwned::Borrowed(v) => MaybeOwned::Owned(Box::from(v.to_vec())),
            },
            prev: match &self.prev {
                None => None,
                Some(MaybeOwned::Owned(v)) => Some(MaybeOwned::Owned(Box::new(v.close()))),
                Some(MaybeOwned::Borrowed(v)) => Some(MaybeOwned::Owned(Box::new(v.close()))),
            },
            propogating_data: self.propogating_data.to_owned(),
            id: self.id,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct BeatTimePair {
    time: SafeFloat,
    beat: SafeFloat,
}

impl BeatTimePair {
    fn new(time: SafeFloat, beat: SafeFloat) -> BeatTimePair {
        BeatTimePair { time, beat }
    }

    pub fn time(self) -> SafeFloat {
        self.time
    }

    pub fn beat(self) -> SafeFloat {
        self.beat
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Query {
    Beat(SafeFloat),
    Time(SafeFloat),
}

impl Query {
    fn linear_approximation(
        self,
        below: BeatTimePair,
        above: BeatTimePair,
    ) -> Result<BeatTimePair, RuntimeErrorType> {
        Ok(match self {
            Query::Beat(beat) => BeatTimePair::new(
                SafeFloat::try_new_no_span(lerp(
                    beat.inner(),
                    below.beat().inner(),
                    above.beat().inner(),
                    below.time().inner(),
                    above.time().inner(),
                ))?,
                beat,
            ),
            Query::Time(time) => BeatTimePair::new(
                time,
                SafeFloat::try_new_no_span(lerp(
                    time.inner(),
                    below.time().inner(),
                    above.time().inner(),
                    below.beat().inner(),
                    above.beat().inner(),
                ))?,
            ),
        })
    }
}

fn lerp(v: f64, from_0: f64, from_1: f64, to_0: f64, to_1: f64) -> f64 {
    if from_1 == from_0 {
        return to_0; // If from_1 == from_0, then to_1 should equal to_0 given how this is being used
    }

    (to_0 * (from_1 - v) + to_1 * (v - from_0)) / (from_1 - from_0)
}

const PRECISION: f64 = 0.1;

pub struct BeatTimeMap {
    beat_time_list: Vec<BeatTimePair>,
}

impl BeatTimeMap {
    pub fn new<'a>(
        bpm: WithSpan<'a, &ClosureOrValue<'a>>,
        lights_amt: usize,
        max_time: f64,
    ) -> Result<Self, RuntimeError<'a>> {
        let len = (max_time / PRECISION) as usize;

        let mut time = SafeFloat::from_i32(0);
        let mut beat = SafeFloat::from_i32(0);
        let beat_time_list = Vec::with_capacity(len + 1);

        let mut this = Self { beat_time_list };

        // Split the addition of the beat in half for trapezoidal approximation, hopefully more accurate

        this.beat_time_list.push(BeatTimePair { time, beat });

        let stack = VarStack::new_base(beat, time, SafeFloat::from_usize(lights_amt));

        let calculated_bpm: f64 = match bpm.execute(&stack, &this) {
            Ok(v) => Value::expect_number(&v, bpm.span())?.inner(),
            Err(e) => return Err(e),
        };

        let mut half_d_beat = PRECISION * calculated_bpm / 120.;

        for _ in 0..len {
            beat = SafeFloat::try_new_no_span(beat.inner() + half_d_beat)
                .expect("Adding not to break the float");

            let stack = VarStack::new_base(beat, time, SafeFloat::from_usize(lights_amt));

            let bpm: f64 = match bpm.execute(&stack, &this) {
                Ok(v) => Value::expect_number(&v, bpm.span())?.inner(),
                Err(e) => return Err(e),
            };

            half_d_beat = PRECISION * bpm / 120.;

            time = SafeFloat::try_new_no_span(time.inner() + PRECISION)
                .expect("Adding not to break the float");

            beat = SafeFloat::try_new_no_span(beat.inner() + half_d_beat)
                .expect("Adding not to break the float");

            this.beat_time_list.push(BeatTimePair { time, beat });
        }

        Ok(this)
    }

    pub fn query<'a>(
        &self,
        query: WithSpan<'a, Query>,
    ) -> Result<WithSpan<'a, BeatTimePair>, RuntimeError<'a>> {
        let (start, end) = match query.into_val() {
            Query::Beat(beat) => {
                let mut start: usize = 0;
                let mut end: usize = self.beat_time_list.len() - 1;

                if self.beat_time_list[start].beat() > beat
                    || self.beat_time_list[end].beat() < beat
                {
                    return Err(query
                        .span()
                        .with(RuntimeErrorType::TimeQueryOutOfRange(query.into_val()))
                        .into());
                }

                while start.abs_diff(end) > 1 {
                    let midpoint = (start + end) >> 1;

                    if self.beat_time_list[midpoint].beat() < beat {
                        start = midpoint;
                    } else {
                        end = midpoint
                    }
                }

                (start, end)
            }
            Query::Time(time) => {
                let pos = time.inner() / PRECISION;

                if pos.floor() < 0. || pos.ceil() >= self.beat_time_list.len() as f64 {
                    return Err(query
                        .span()
                        .with(RuntimeErrorType::TimeQueryOutOfRange(query.into_val()))
                        .into());
                }

                (pos.floor() as usize, pos.ceil() as usize)
            }
        };

        match query.linear_approximation(self.beat_time_list[start], self.beat_time_list[end]) {
            Ok(v) => Ok(query.span().with(v)),
            Err(e) => Err(query.span().with(e).into()),
        }
    }
}
