use std::mem;
use std::rc::Rc;

use crate::compiler::NativeFn;
use crate::utils::error;
use crate::utils::merge_spans;
use crate::utils::SpanExt;
use once_cell::sync::Lazy;
use pest::{
    error::Error,
    iterators::{Pair, Pairs},
    pratt_parser::PrattParser,
    Parser, Span,
};
use pest_derive::Parser;

use crate::utils::WithSpan;

#[derive(Parser)]
#[grammar = "./grammar.pest"]
struct CodeParser;

#[derive(Clone, Debug)]
pub enum Expr<'a> {
    Sequence(Sequence<'a>),
    FnCall(FnCall<'a>),
    Statements(Statements<'a>),
    Number(f64),
    List(Vec<WithSpan<'a, Expr<'a>>>),
    UnaryExpr(Box<UnaryExpr<'a>>),
    BinaryExpr(Box<BinaryExpr<'a>>),
    Close(WithSpan<'a, &'a str>),
    Closure(Box<Closure<'a>>),
}

#[derive(Clone, Copy, Debug)]
pub enum SequenceBlendType {
    Switch,
    Add,
}

#[derive(Clone, Debug)]
pub struct Sequence<'a> {
    pub elements: Vec<SequenceElement<'a>>,
    pub blend_type: WithSpan<'a, SequenceBlendType>,
}

#[derive(Clone, Debug)]
pub struct SequenceElement<'a> {
    pub expr: WithSpan<'a, Expr<'a>>,
    pub time: WithSpan<'a, Expr<'a>>,
}

#[derive(Clone, Debug)]
pub struct FnCall<'a> {
    pub name: WithSpan<'a, &'a str>,
    pub args: Vec<WithSpan<'a, Expr<'a>>>,
}

#[derive(Clone, Debug)]
pub struct Def<'a> {
    pub name: WithSpan<'a, &'a str>,
    pub params: WithSpan<'a, Rc<[WithSpan<'a, &'a str>]>>,
    pub expr: WithSpan<'a, Expr<'a>>,
}

#[derive(Clone, Debug)]
pub struct Closure<'a> {
    pub params: WithSpan<'a, Rc<[WithSpan<'a, &'a str>]>>,
    pub expr: WithSpan<'a, Expr<'a>>,
}

#[derive(Clone, Debug)]
pub struct Statements<'a> {
    pub defs: Vec<WithSpan<'a, Def<'a>>>,
    pub expr: Box<WithSpan<'a, Expr<'a>>>,
}

#[derive(Clone, Copy, Debug)]
pub enum UnaryOp {
    Neg,
}

impl UnaryOp {
    pub fn native(self) -> NativeFn {
        match self {
            Self::Neg => crate::stdlib::neg,
        }
    }
}

#[derive(Clone, Debug)]
pub struct UnaryExpr<'a> {
    pub op: WithSpan<'a, UnaryOp>,
    pub expr: WithSpan<'a, Expr<'a>>,
}

#[derive(Clone, Copy, Debug)]
pub enum BinaryOp {
    Rem,
    Add,
    Sub,
    Mul,
    Div,
    Exp,
}

impl BinaryOp {
    pub fn native(self) -> NativeFn {
        use BinaryOp::*;

        match self {
            Rem => crate::stdlib::rem,
            Add => crate::stdlib::add,
            Sub => crate::stdlib::sub,
            Mul => crate::stdlib::mul,
            Div => crate::stdlib::div,
            Exp => crate::stdlib::exp,
        }
    }
}

#[derive(Clone, Debug)]
pub struct BinaryExpr<'a> {
    pub lhs: WithSpan<'a, Expr<'a>>,
    pub op: WithSpan<'a, BinaryOp>,
    pub rhs: WithSpan<'a, Expr<'a>>,
}

static PRATT_PARSER: Lazy<PrattParser<Rule>> = Lazy::new(|| {
    use pest::pratt_parser::{Assoc::*, Op};
    use Rule::*;

    PrattParser::new()
        .op(Op::infix(rem, Left))
        .op(Op::infix(add, Left) | Op::infix(sub, Left))
        .op(Op::infix(mul, Left) | Op::infix(div, Left))
        .op(Op::infix(exp, Right))
});

fn parse_params(params: Pair<'_, Rule>) -> WithSpan<'_, Rc<[WithSpan<'_, &'_ str>]>> {
    params.as_span().with(
        params
            .into_inner()
            .map(|v| v.as_span().with(v.as_str()))
            .collect(),
    )
}

fn parse_def(primary: Pair<'_, Rule>) -> Result<WithSpan<'_, Def<'_>>, Box<Error<Rule>>> {
    let primary_span = primary.as_span();

    let mut pairs = primary.into_inner();

    let Some(name) = pairs.next() else {
        return Err(error("Expected an identifier", primary_span));
    };
    let Some(params) = pairs.next() else {
        return Err(error("Expected parameters", name.as_span()));
    };
    let Some(expr) = pairs.next() else {
        return Err(error(
            "Expected an expression after the name",
            params.as_span(),
        ));
    };

    Ok(primary_span.with(Def {
        name: name.as_span().with(name.as_str()),
        params: parse_params(params),
        expr: parse_expr(expr)?,
    }))
}

fn parse_statements<'a>(
    span: Span<'a>,
    mut statements: Pairs<'a, Rule>,
) -> Result<WithSpan<'a, Statements<'a>>, Box<Error<Rule>>> {
    let Some(final_expr) = statements.next_back() else {
        return Err(error("Expected a final statement", span));
    };

    Ok(span.with(Statements {
        defs: statements.map(parse_def).collect::<Result<_, _>>()?,
        expr: Box::new(parse_expr(final_expr)?),
    }))
}

fn parse_blend_type<'a>(
    blend_type_pair: &Pair<'a, Rule>,
) -> Result<WithSpan<'a, SequenceBlendType>, Box<Error<Rule>>> {
    let span = blend_type_pair.as_span();

    match blend_type_pair.as_str() {
        "switch" => Ok(span.with(SequenceBlendType::Switch)),
        "add" => Ok(span.with(SequenceBlendType::Add)),
        _ => Err(error(
            "Expected the sequence blend type to be \"switch\" or \"add\"",
            blend_type_pair.as_span(),
        )),
    }
}

fn parse_expr(pair: Pair<'_, Rule>) -> Result<WithSpan<'_, Expr<'_>>, Box<Error<Rule>>> {
    PRATT_PARSER
        .map_primary(|primary| {
            let span = primary.as_span();

            match primary.as_rule() {
                Rule::sequence => {
                    let mut pairs = primary.into_inner();

                    let Some(blend_type_pair) = pairs.next_back() else {
                        return Err(error("Expected there to be a blend type", span));
                    };

                    let blend_type = parse_blend_type(&blend_type_pair)?;

                    let Some(final_elt) = pairs.next_back() else {
                        return Err(error("Expected there to be a final sequence element", span));
                    };

                    let mut elements = Vec::new();

                    loop {
                        let Some(expr) = pairs.next() else { break };
                        let Some(time) = pairs.next() else {
                            return Err(error("Expected an amount of time next", expr.as_span()));
                        };

                        elements.push(SequenceElement {
                            expr: parse_expr(expr)?,
                            time: parse_expr(time)?,
                        });
                    }

                    elements.push(SequenceElement {
                        time: final_elt.as_span().with(Expr::Number(f64::MAX)),
                        expr: parse_expr(final_elt)?,
                    });

                    Ok(span.with(Expr::Sequence(Sequence {
                        blend_type,
                        elements,
                    })))
                }
                Rule::track => {
                    let mut pairs = primary.into_inner();

                    let Some(track_data_pair) = pairs.next_back() else {
                        return Err(error("Expected a track data section", span));
                    };

                    let mut track_data = track_data_pair.into_inner();

                    let Some(beats_per_space_pair) = track_data.next() else {
                        return Err(error("Expected beats per space", span));
                    };
                    let beats_per_space = parse_expr(beats_per_space_pair)?;

                    fn track_section_to_element<'a>(
                        beats_per_space: WithSpan<'a, Expr<'a>>,
                        e: (WithSpan<'a, Expr<'a>>, WithSpan<'a, f64>),
                    ) -> SequenceElement<'a> {
                        SequenceElement {
                            expr: e.0,
                            time: e.1.span().with(Expr::BinaryExpr(Box::new(BinaryExpr {
                                lhs: e.1.span().with(Expr::Number(e.1.into_val())),
                                op: beats_per_space.span().with(BinaryOp::Mul),
                                rhs: beats_per_space,
                            }))),
                        }
                    }

                    let Some(blend_type_pair) = track_data.next() else {
                        return Err(error("Expected blend type", span));
                    };
                    let blend_type = parse_blend_type(&blend_type_pair)?;

                    let (mut elements, last_elt) = pairs.try_fold(
                        (Vec::new(), None::<(WithSpan<Expr>, WithSpan<f64>)>),
                        |(mut elements, mut element_building), pair| {
                            let pair_span = pair.as_span();
                            let pair_str = pair.as_str();

                            match (&mut element_building, pair.as_rule()) {
                                (Some(e), Rule::space_in_track) => {
                                    let (val, span) = e.1.into_inner();
                                    e.1 = merge_spans(span, pair_span).with(val + 1.);
                                }
                                (Some(e), _) => {
                                    elements.push(track_section_to_element(
                                        beats_per_space.to_owned(),
                                        mem::replace(
                                            e,
                                            (
                                                parse_expr(pair)?,
                                                pair_span.with(pair_str.len() as f64),
                                            ),
                                        ),
                                    ));
                                }
                                (None, Rule::space_in_track) => {
                                    return Err(error(
                                        "A track can't start with a space",
                                        pair.as_span(),
                                    ))
                                }
                                (None, _) => {
                                    element_building = Some((
                                        parse_expr(pair)?,
                                        pair_span.with(pair_str.len() as f64),
                                    ))
                                }
                            }

                            Ok((elements, element_building))
                        },
                    )?;

                    if let Some(last_elt) = last_elt {
                        elements.push(track_section_to_element(beats_per_space, last_elt));
                    }

                    let Some(last) = elements.last_mut() else {
                        return Err(error("The track must have at least one element", span));
                    };

                    last.time = last.time.span().with(Expr::Number(f64::MAX));

                    Ok(span.with(Expr::Sequence(Sequence {
                        elements,
                        blend_type,
                    })))
                }
                Rule::fn_call => {
                    let mut pairs = primary.into_inner();

                    let Some(name) = pairs.next() else {
                        return Err(error("Expected an identifier", span));
                    };

                    let args = pairs
                        .map(parse_expr)
                        .collect::<Result<Vec<_>, Box<Error<crate::parser::Rule>>>>()?;

                    Ok(span.with(Expr::FnCall(FnCall {
                        name: name.as_span().with(name.as_str()),
                        args,
                    })))
                }
                Rule::statements => {
                    parse_statements(span, primary.into_inner()).map(|v| v.map(Expr::Statements))
                }
                Rule::number => Ok(span.with(Expr::Number(
                    primary
                        .as_str()
                        .parse::<f64>()
                        .map_err(|e| error(e.to_string(), span))?,
                ))),
                Rule::unary_expr => {
                    let mut pairs = primary.into_inner();

                    let Some(op) = pairs.next() else {
                        return Err(error("Expected an operator", span));
                    };
                    let Some(expr) = pairs.next() else {
                        return Err(error(
                            "Expected an expression after the unary operator",
                            op.as_span(),
                        ));
                    };

                    Ok(span.with(Expr::UnaryExpr(Box::new(UnaryExpr {
                        op: match op.as_rule() {
                            Rule::neg => op.as_span().with(UnaryOp::Neg),
                            _ => return Err(error("Expected a unary operator", op.as_span())),
                        },
                        expr: parse_expr(expr)?,
                    }))))
                }
                Rule::list => {
                    let pairs = primary.into_inner();

                    Ok(span.with(Expr::List(
                        pairs.map(parse_expr).collect::<Result<Vec<_>, _>>()?,
                    )))
                }
                Rule::close => {
                    let Some(ident) = primary.into_inner().next() else {
                        return Err(error("Expected an indentifier", span));
                    };

                    Ok(span.with(Expr::Close(ident.as_span().with(ident.as_str()))))
                }
                Rule::closure => {
                    let mut pairs = primary.into_inner();

                    let Some(params) = pairs.next() else {
                        return Err(error("Expected parameters", span));
                    };
                    let Some(expr) = pairs.next() else {
                        return Err(error("Expected an expression", span));
                    };

                    Ok(span.with(Expr::Closure(Box::new(Closure {
                        params: parse_params(params),
                        expr: parse_expr(expr)?,
                    }))))
                }
                _ => Err(error("Expected an expression", primary.as_span())),
            }
        })
        .map_infix(|lhs, op, rhs| {
            let lhs = lhs?;
            let rhs = rhs?;

            Ok(lhs
                .span()
                .start_pos()
                .span(&rhs.span().end_pos())
                .with(Expr::BinaryExpr(Box::new(BinaryExpr {
                    lhs,
                    op: op.as_span().with(match op.as_rule() {
                        Rule::rem => BinaryOp::Rem,
                        Rule::add => BinaryOp::Add,
                        Rule::sub => BinaryOp::Sub,
                        Rule::mul => BinaryOp::Mul,
                        Rule::div => BinaryOp::Div,
                        Rule::exp => BinaryOp::Exp,
                        _ => return Err(error("Expected a binary operator", op.as_span())),
                    }),
                    rhs,
                }))))
        })
        .parse(pair.into_inner())
}

pub fn parse_script(script: &str) -> Result<WithSpan<'_, Statements<'_>>, Box<Error<Rule>>> {
    let mut parsed = CodeParser::parse(Rule::script, script)?;
    let Some(statements) = parsed.next() else {
        return Err(error(
            "Expected statements",
            Span::new(script, 0, script.len()).unwrap(),
        ));
    };

    parse_statements(
        Span::new(script, 0, script.len()).expect("0..len is a valid index into script"),
        statements.into_inner(),
    )
}
