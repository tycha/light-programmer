use std::{
    cmp::Ordering,
    collections::HashSet,
    fmt::{Debug, Display},
    ops::{Deref, DerefMut},
};

use pest::error::Error;
pub use pest::Span;
use thiserror::Error;

use crate::interpreting::Query;

pub fn error(message: impl Into<String>, span: Span) -> Box<Error<crate::parser::Rule>> {
    Box::new(Error::new_from_span(
        pest::error::ErrorVariant::CustomError {
            message: message.into(),
        },
        span,
    ))
}

#[derive(Clone, Copy)]
pub struct WithSpan<'a, V>(V, Span<'a>);

impl<'a, V: Debug> Debug for WithSpan<'a, V> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.0, f)
    }
}

impl<'a, V: PartialEq> PartialEq for WithSpan<'a, V> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<'a, V: Eq> Eq for WithSpan<'a, V> {}

impl<'a, V: PartialOrd> PartialOrd for WithSpan<'a, V> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<'a, V: Ord> Ord for WithSpan<'a, V> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

pub fn merge_spans<'a>(lhs: Span<'a>, rhs: Span<'a>) -> Span<'a> {
    let (lhs_start, lhs_end) = lhs.split();
    let (rhs_start, rhs_end) = rhs.split();

    let start = lhs_start.min(rhs_start);
    let end = lhs_end.max(rhs_end);

    start.span(&end)
}

impl<'a, V> WithSpan<'a, V> {
    pub fn span(&self) -> Span<'a> {
        self.1
    }

    pub fn into_inner(self) -> (V, Span<'a>) {
        (self.0, self.1)
    }

    pub fn into_val(self) -> V {
        self.0
    }

    pub fn map<T>(self, map: impl FnOnce(V) -> T) -> WithSpan<'a, T> {
        WithSpan(map(self.0), self.1)
    }

    pub fn try_map<T, E>(self, map: impl FnOnce(V) -> Result<T, E>) -> Result<WithSpan<'a, T>, E> {
        Ok(WithSpan(map(self.0)?, self.1))
    }

    pub fn as_ref<'c>(&'c self) -> WithSpan<'a, &'c V> {
        WithSpan(&self.0, self.1)
    }

    pub fn as_mut<'c>(&'c mut self) -> WithSpan<'a, &'c mut V> {
        WithSpan(&mut self.0, self.1)
    }
}

impl<'a, T> WithSpan<'a, Option<T>> {
    pub fn transpose(self) -> Option<WithSpan<'a, T>> {
        let (val, span) = self.into_inner();
        val.map(|v| span.with(v))
    }
}

impl<'a, V> Deref for WithSpan<'a, V> {
    type Target = V;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'a, V> DerefMut for WithSpan<'a, V> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub trait SpanExt<'a>: Sized {
    fn with<T>(self, thing: T) -> WithSpan<'a, T>;
}

impl<'a> SpanExt<'a> for Span<'a> {
    fn with<T>(self, thing: T) -> WithSpan<'a, T> {
        WithSpan(thing, self)
    }
}

pub struct RuntimeError<'a> {
    span: Span<'a>,
    ty: RuntimeErrorType,
}

impl<'a> Display for RuntimeError<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.as_pest_err(), f)
    }
}

impl<'a> RuntimeError<'a> {
    fn as_pest_err(&self) -> Box<pest::error::Error<crate::parser::Rule>> {
        error(self.ty.to_string(), self.span)
    }
}

#[derive(Debug, Error)]
pub enum RuntimeErrorType {
    #[error("Type error: expected {expected}, found {got}")]
    TypeError { expected: String, got: String },
    #[error("Tried to do an operation on lists of different lengths: {0:?}")]
    ListsDifferentLengths(HashSet<usize>),
    #[error("Invalid floating point error: {0}")]
    SafeFloat(#[from] SafeFloatError),
    #[error("Incorrect arguments to function: Expected {0}, got {1}")]
    IncorrectArgs(usize, usize),
    #[error("Time query out of range of the expected time inputs: {0:?}")]
    TimeQueryOutOfRange(Query),
}

impl<'a> From<RuntimeError<'a>> for Box<pest::error::Error<crate::parser::Rule>> {
    fn from(value: RuntimeError) -> Self {
        value.as_pest_err()
    }
}

impl<'a> Debug for RuntimeError<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.as_pest_err(), f)
    }
}

impl<'a> From<WithSpan<'a, RuntimeErrorType>> for RuntimeError<'a> {
    fn from(value: WithSpan<'a, RuntimeErrorType>) -> Self {
        let (ty, span) = value.into_inner();

        RuntimeError { span, ty }
    }
}

#[derive(Debug, Error)]
pub enum SafeFloatError {
    #[error("Created a value that is NaN")]
    NumberIsNaN,
    #[error("Created a value that is infinite")]
    NumberIsInfinite,
}

#[derive(Clone, Copy)]
pub struct SafeFloat(f64);

impl Debug for SafeFloat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.0, f)
    }
}

impl PartialEq for SafeFloat {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl PartialOrd for SafeFloat {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for SafeFloat {}

impl Ord for SafeFloat {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0
            .partial_cmp(&other.0)
            .expect("SafeFloat not to contain NaN or Infinity")
    }
}

impl SafeFloat {
    pub const fn from_i32(int: i32) -> SafeFloat {
        SafeFloat(int as f64)
    }

    pub const fn from_usize(int: usize) -> SafeFloat {
        SafeFloat(int as f64)
    }

    pub fn try_new_no_span(f: f64) -> Result<SafeFloat, RuntimeErrorType> {
        if f.is_nan() {
            return Err(SafeFloatError::NumberIsNaN.into());
        }

        if f.is_infinite() {
            return Err(SafeFloatError::NumberIsInfinite.into());
        }

        Ok(SafeFloat(f))
    }

    pub const fn inner(self) -> f64 {
        self.0
    }

    pub fn try_new(f: WithSpan<'_, f64>) -> Result<WithSpan<'_, SafeFloat>, RuntimeError<'_>> {
        f.try_map(Self::try_new_no_span)
            .map_err(|e| f.span().with(e).into())
    }
}

impl<'a> WithSpan<'a, SafeFloat> {
    pub fn map_float(
        self,
        func: impl FnOnce(f64) -> f64,
    ) -> Result<WithSpan<'a, SafeFloat>, RuntimeError<'a>> {
        SafeFloat::try_new(self.map(|v| func(v.inner())))
    }
}
